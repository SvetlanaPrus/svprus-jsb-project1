//time_01..time_03 относятся к сегодняшнему дню,
//time_04..time_06 к завтрашнему.

export function isTomorrow(str){
    let last = str.split('').pop();
    return last >= 4;
}
