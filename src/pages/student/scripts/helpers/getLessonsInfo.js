import {lessons} from "../constants";

// const lessons = {
//     type_01: {
//         title: 'Новый урок',
//         duration: 120
//     }, ...
// };

export function getLessonsInfo(key){
    const arr = Object.entries(lessons);
    let result;

    arr.map(el => {
        if(el[0] === key) result = el[1];
    });

    return result;
}
