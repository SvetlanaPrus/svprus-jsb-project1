import {timeSlots} from "../constants";

// const timeSlots = {
//     time_01: 12,
//     time_02: 14,
//     time_03: 16,
//     time_04: 12,
//     time_05: 14,
//     time_06: 16,
// };

// FUNCTION WAS NOT USED!!!

export function setTime(key){
    // console.log('my key:', key);
    const arr = Object.entries(timeSlots);
    // console.log('my arr:', arr);

    let result = arr.map((el,i) => {
        if(el[0] === key) return el[1];
        // console.log(el[1]);
    });

    return result;
}
