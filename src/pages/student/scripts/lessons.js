import {timeSlots} from "./constants";
import {isTomorrow} from "./helpers/isTomorrow";
import {studentName} from "./helpers/getStudentsName";
import {getLessonsInfo} from "./helpers/getLessonsInfo";

// Get lesson's information: TITLE & DURATION -

const arrayType = document.getElementsByClassName('type-box');
let details;

for(let i = 0; i < arrayType.length; i++){
    arrayType[i].addEventListener('click', () =>{

        let inputTypeElement = arrayType[i].getElementsByTagName('input');

        let inputTypeId = inputTypeElement[0].id;
        details = getLessonsInfo(inputTypeId);
    });
}

// Get other information: NAME & TIME & TOMORROW -

const array = document.getElementsByClassName('time-slot');
let hour, isNotToday;

for(let i = 0; i < array.length; i++){
    array[i].addEventListener('click', () =>{
        let inputElement = array[i].getElementsByTagName('input');

        let inputId = inputElement[0].id;

        hour = timeSlots[inputId];
        isNotToday = isTomorrow(inputId);

        // oneLesson = {
        //     name: studentName(),
        //     time: timeSlots[inputId],
        //     tomorrow: isTomorrow(inputId),
        //     title: details.title,
        //     duration: details.duration,
        // };
        // console.log(oneLesson);
    });
}

// Кнопка "Забукать занятие" (submit)

const divOrderLesson = document.getElementsByClassName("button-wrapper");
// console.log(divOrderLesson, 'divOrderLesson');

const btnOrderLesson = divOrderLesson[0].getElementsByClassName("button");
console.log(btnOrderLesson, 'btnOrderLesson');

let oneLesson = {};

btnOrderLesson[0].addEventListener('click', () =>{
    oneLesson = {
        name: studentName(),
        time: hour,
        tomorrow: isNotToday,
        title: details.title,
        duration: details.duration,
    };

    console.log(oneLesson);

    let existingLessons = JSON.parse(localStorage.getItem("lessons"));
    if (existingLessons == null){
        let setLessons = [];
        setLessons.push(oneLesson);
        localStorage.setItem("lessons", JSON.stringify(setLessons));
    } else {
        existingLessons.push(oneLesson);
        localStorage.setItem("lessons", JSON.stringify(existingLessons));
    }
    oneLesson = {};
});














