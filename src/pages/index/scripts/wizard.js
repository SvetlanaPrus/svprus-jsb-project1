import {loginBtnClick, saveToStorage} from './login';
import {isObjEmpty} from "./helpers/isObjEmpty";
import {isStringShort} from "./helpers/isStringShort";
import {isStringsEqual} from "./helpers/isStringsEqual";

let students = [];
let teacher = {};

// ФОРМА "ВОЙТИ В ШКОЛУ" - nr.1
const regBtn = document.querySelector("#regBtn");
const loginBtn = document.querySelector('#loginBtn');
const divOld = document.querySelector("#loginBlock");
const divNext = document.querySelector("#step1Block");

// кнопка "зарегистрироваться"
regBtn.addEventListener("click", (event) => {
    divNext.style.display = "flex";
    // заменяем существующий элемент divOld на divNext:
    divOld.parentNode.replaceChild(divNext, divOld);
});

// кнопка "Войти в систему"
loginBtn.addEventListener("click", (event) => {
    loginBtnClick();
});


// ФОРМА "КТО ТЫ - СТУДЕНТ ИЛИ УЧИТЕЛЬ?" - nr.2
let isStudentChecked = document.querySelector('#user_student').checked;
let isTeacherChecked = document.querySelector('#user_teacher').checked;

// Кнопки выбора - "студент" или "учитель".
const btnStudent = document.querySelector("#user_student");
const btnTeacher = document.querySelector("#user_teacher");

btnStudent.addEventListener("click", (event) => {
    isStudentChecked = true;
    isTeacherChecked = false;
});

btnTeacher.addEventListener("click", (event) => {
    if (isObjEmpty(teacher) === true) {
        isTeacherChecked = true;
        isStudentChecked = false;
    } else {
        alert('Учитель уже зарегистрирован.');
    }
});

// Возврат на первую страницу
const btnFromBlock2 = document.querySelector("#toLoginSvg");

btnFromBlock2.addEventListener("click", (event) => {
    if (isStudentChecked) {
        document.querySelector('#user_student').checked = false;
    } else {
        document.querySelector("#user_teacher").checked = false;
    }

    divOld.style.display = "flex";
    divNext.parentNode.replaceChild(divOld, divNext);
});

// кнопка "продолжить"
const toStep2Btn = document.querySelector("#toStep2Btn");
const divLast = document.querySelector("#regBlock");
let newStudent = {};

toStep2Btn.addEventListener("click", (event) => {
    if (isStudentChecked) {
        newStudent.type = 'student';
    } else {
        teacher.type = 'teacher';
    }

    divLast.style.display = "flex";
    divNext.parentNode.replaceChild(divLast, divNext);
});


// ФОРМА "ЗАРЕГИСТРИРУЙСЯ СЕЙЧАС!" - nr.3

// Вернуться к предыдущей форме "кто ты?"
const btnFromBlock3 = document.querySelector("#from3to2Svg");

btnFromBlock3.addEventListener("click", (event) => {
    if (isStudentChecked) {
        delete newStudent.type;
    } else {
        delete teacher.type;
    }

    divNext.style.display = "flex";
    divLast.parentNode.replaceChild(divNext, divLast);
});

// Кнопка "создать аккаунт" (submit)
const btnCreateAccount = document.querySelector("#createAccount");
const inputName = document.querySelector("#name");
const inputPassword = document.querySelector("#password");
const inputPasswordNext = document.querySelector("#password_next");

btnCreateAccount.addEventListener("click", (event) => {
    event.preventDefault();

    const name = inputName.value;
    const email = document.querySelector("#email").value;
    const password = inputPassword.value;
    const passwordNext = inputPasswordNext.value;

    if(isStringsEqual(password, passwordNext) !== true){
        inputPassword.value = '';
        inputPasswordNext.value = '';
        inputPassword.classList.add('error');
        inputPasswordNext.classList.add('error');

        divLast.style.display = "flex";
        divNext.parentNode.replaceChild(divLast, divNext);
    }

    if (isStringShort(name) === true) {
        inputName.value = '';
        // inputName.classList.add('error');  // тоже рабочий вариант. класс "error" уже прописан в CSS.
        inputName.style.borderColor = 'red';
        inputName.placeholder = '*Слова должны иметь минимум три символа';
        inputName.addEventListener('click', (event) => {
            inputName.style.borderColor = '#eaeaea';
            inputName.placeholder = 'Имя и фамилия';
        });

        divLast.style.display = "flex";
        divNext.parentNode.replaceChild(divLast, divNext);
    } else {
        if (isStudentChecked) {
            newStudent.name = name;
            newStudent.email = email;
            newStudent.password = password;

            let existingStudents = JSON.parse(localStorage.getItem("students"));
            if(existingStudents == null) existingStudents = [];
            existingStudents.push(newStudent);
            localStorage.setItem("students", JSON.stringify(existingStudents));
            newStudent = {};

            saveToStorage(existingStudents[existingStudents.length - 1]);      // запись в "login"
            window.location.href = 'student.html';
        } else {
            teacher.name = name;
            teacher.email = email;
            teacher.password = password;

            let existingTeacher = JSON.parse(localStorage.getItem("teacher"));
            if(existingTeacher == null) existingTeacher = {};
            localStorage.setItem("teacher", JSON.stringify(teacher));

            saveToStorage(teacher);
            window.location.href = 'teacher.html';
        }

        const form = document.querySelector(".form-login");
        form.reset();
    }
});

















